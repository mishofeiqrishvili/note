package com.example.viewfragmentadapter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {
    private lateinit var editTextNote : EditText
    private lateinit var Add : Button
    private lateinit var textView : TextView
    private lateinit var tablayout : TabLayout
    private lateinit var viewpager : ViewPager2
    private lateinit var ViewPagerFragmentAdapter : ViewPagerFragmentAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val sharedpreferences = getSharedPreferences("MY_NOTE", MODE_PRIVATE)
        val text = sharedpreferences.getString("NOTE","")
        textView.text = text

        editTextNote = findViewById(R.id.editTextNote)
        Add = findViewById(R.id.Add)
        textView = findViewById(R.id.textView)
        tablayout = findViewById(R.id.tablayout)
        viewpager = findViewById(R.id.viewpager)
        ViewPagerFragmentAdapter = ViewPagerFragmentAdapter(this)

        viewpager.adapter = ViewPagerFragmentAdapter
        TabLayoutMediator(tablayout,viewpager){tab, position ->
            tab.text = "tab ${position + 1}"

        }.attach()

        Add.setOnClickListener {
            val note = editTextNote.text.toString()
            val text = textView.text.toString()
             val result = note + "\n" + text

            textView.text = result

            sharedpreferences.edit().putString("NOTE",result).apply()
        }
    }
}